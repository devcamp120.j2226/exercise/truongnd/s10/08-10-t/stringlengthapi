package com.devcamp.stringrestapi;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
@CrossOrigin
public class StringRestApiController {
    @GetMapping("/length")
    public ArrayList<String> spiltString(@RequestParam(required = true,name = "string")String requetsString){
        ArrayList<String> spiltStringArray = new ArrayList<>();
        
        String[] stringArray = requetsString.split(" ");
        System.out.println("Chieu dai la: " + stringArray.length);
        for (String stringElement : stringArray) {
            spiltStringArray.add(stringElement);
        }

        return spiltStringArray;
    }
}
